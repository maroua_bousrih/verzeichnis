package org.campus.fileSystem;

public class Datei
{
	private String name;
	private int gross;
	
	public Datei(String name, int gross)
	{
		this.name=name;
		this.gross=gross;
	}
	
	public String toString()
	{
		return String.format("[name: %s, gross: %d]", name,gross);
	}

	public String getName()
	{
		return name;
	}

	public int getGross()
	{
		return gross;
	}
	

}
